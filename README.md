# RTPAgent_LOG

RTPAgent log scripts for Debian and Centos

### DEBIAN

run `rtpagent_log_deb.sh`

### CENTOS 7

run `rtpagent_log_el7.sh`


The scripts generate a unique zip (or tar) file with all the RTPAgent information for troubleshooting
